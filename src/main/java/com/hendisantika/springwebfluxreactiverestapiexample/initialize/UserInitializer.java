package com.hendisantika.springwebfluxreactiverestapiexample.initialize;

import com.hendisantika.springwebfluxreactiverestapiexample.entity.Department;
import com.hendisantika.springwebfluxreactiverestapiexample.entity.User;
import com.hendisantika.springwebfluxreactiverestapiexample.repository.DepartmentRepository;
import com.hendisantika.springwebfluxreactiverestapiexample.repository.UserRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Flux;

import java.util.Arrays;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-webflux-reactive-rest-api-example
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 08/02/21
 * Time: 07.57
 */
@Component
@Profile("!test")
@Slf4j
public class UserInitializer implements CommandLineRunner {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private DepartmentRepository departmentRepository;

    @Override
    public void run(String... args) {
        initialDataSetup();
    }

    private List<User> getData() {
        return Arrays.asList(new User(null, "Uzumaki Naruto", 30, 10000),
                new User(null, "Uchiha Sasuke", 5, 1000),
                new User(null, "Uchiha Madara", 5, 1000),
                new User(null, "Uchiha Itachi", 5, 1000),
                new User(null, "Hatake Kakashi", 5, 1000),
                new User(null, "Haruno Sakura", 40, 1000000));
    }

    private List<Department> getDepartments() {
        return Arrays.asList(new Department(null, "Mechanical", 1, "Konoha"),
                new Department(null, "Computer", 2, "Kirigakure"));
    }

    private void initialDataSetup() {
        userRepository.deleteAll()
                .thenMany(Flux.fromIterable(getData()))
                .flatMap(userRepository::save)
                .thenMany(userRepository.findAll())
                .subscribe(user -> {
                    log.info("User Inserted from CommandLineRunner " + user);
                });

        departmentRepository.deleteAll()
                .thenMany(Flux.fromIterable(getDepartments()))
                .flatMap(departmentRepository::save)
                .thenMany(departmentRepository.findAll())
                .subscribe(user -> {
                    log.info("Department Inserted from CommandLineRunner " + user);
                });

    }
}
