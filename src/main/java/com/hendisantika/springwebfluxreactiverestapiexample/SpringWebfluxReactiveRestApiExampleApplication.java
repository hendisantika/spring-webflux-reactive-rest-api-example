package com.hendisantika.springwebfluxreactiverestapiexample;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringWebfluxReactiveRestApiExampleApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringWebfluxReactiveRestApiExampleApplication.class, args);
    }

}
