package com.hendisantika.springwebfluxreactiverestapiexample.repository;

import com.hendisantika.springwebfluxreactiverestapiexample.entity.Department;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import reactor.core.publisher.Mono;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-webflux-reactive-rest-api-example
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 06/02/21
 * Time: 21.09
 */

public interface DepartmentRepository extends ReactiveCrudRepository<Department, Integer> {
    Mono<Department> findByUserId(Integer userId);
}