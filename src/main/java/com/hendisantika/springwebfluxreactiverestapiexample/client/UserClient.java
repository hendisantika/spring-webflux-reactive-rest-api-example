package com.hendisantika.springwebfluxreactiverestapiexample.client;

import com.hendisantika.springwebfluxreactiverestapiexample.entity.User;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-webflux-reactive-rest-api-example
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 08/02/21
 * Time: 08.01
 */
@Component
@Slf4j
public class UserClient {
    private WebClient client = WebClient.create("http://localhost:8080");

    public Mono<User> getUser(String userId) {
        return client.get()
                .uri("/users/{userId}", userId)
                .retrieve()
                .bodyToMono(User.class).log(" User fetched ");
    }

    public Flux<User> getAllUsers() {
        return client.get()
                .uri("/users")
                .exchange().flatMapMany(clientResponse -> clientResponse.bodyToFlux(User.class)).log("Users Fetched :" +
                        " ");
    }

    public Mono<User> createUser(User user) {
        Mono<User> userMono = Mono.just(user);
        return client.post().uri("/users").contentType(MediaType.APPLICATION_JSON)
                .body(userMono, User.class).retrieve().bodyToMono(User.class).log("Created User : ");

    }

}
